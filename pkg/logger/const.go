package logger

// LevelDebug shows all messages which is written in log
// LevelInfo shows info messages
// LevelWarn shows warning messages
// LevelError shows error messages
// LevelPanic shows panic messages and stops service
// LevelFatal shows fatal messages and stops service
const (
	LevelDebug = "debug"
	LevelInfo  = "info"
	LevelWarn  = "warn"
	LevelError = "error"
	LevelPanic = "panic"
	LevelFatal = "fatal"
)
