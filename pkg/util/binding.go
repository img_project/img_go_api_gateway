package util

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

func BindError(err error) error {
	if errs, ok := err.(validator.ValidationErrors); ok {
		for _, err := range errs {
			return fmt.Errorf("'%s' validation failed on the '%s'", err.Field(), err.Tag())
		}
	}

	return fmt.Errorf("could not bind value")
}

func ValidateUUID(id string) bool {
	_, err := uuid.Parse(id)

	return err == nil
}
