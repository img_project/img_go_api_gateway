package util

func InArray(item string, items []string) bool {
	for _, v := range items {
		if v == item {
			return true
		}
	}

	return false
}
