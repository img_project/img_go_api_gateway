package util

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var grpcToHttpCode = map[codes.Code]int{
	codes.OK:                 http.StatusOK,
	codes.Canceled:           http.StatusBadGateway,
	codes.Unknown:            http.StatusInternalServerError,
	codes.InvalidArgument:    http.StatusBadRequest,
	codes.DeadlineExceeded:   http.StatusGatewayTimeout,
	codes.NotFound:           http.StatusNotFound,
	codes.AlreadyExists:      http.StatusInternalServerError,
	codes.PermissionDenied:   http.StatusForbidden,
	codes.ResourceExhausted:  http.StatusInternalServerError,
	codes.FailedPrecondition: http.StatusInternalServerError,
	codes.Aborted:            http.StatusBadGateway,
	codes.OutOfRange:         http.StatusInternalServerError,
	codes.Unimplemented:      http.StatusInternalServerError,
	codes.Internal:           http.StatusInternalServerError,
	codes.Unavailable:        http.StatusMethodNotAllowed,
	codes.DataLoss:           http.StatusInternalServerError,
	codes.Unauthenticated:    http.StatusUnauthorized,
}

const DefaultInternalErrorMessage = "Oops, something went wrong. Please contact us!"

func SuccessResponse(c *gin.Context, statusCode int, data interface{}) {
	logger.Get().Info("response", logger.Any("data", data))
	c.JSON(statusCode, model.SuccessResponse{
		Code:    statusCode,
		Message: http.StatusText(statusCode),
		Data:    data,
	})
}

func ErrorResponse(c *gin.Context, statusCode int, format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)

	if statusCode == http.StatusInternalServerError {
		logger.Get().Error(message)
		message = DefaultInternalErrorMessage
	} else {
		logger.Get().Warn(message)
	}

	response := model.ErrorResponse{
		Code:    statusCode,
		Message: http.StatusText(statusCode),
		Error:   message,
	}

	c.AbortWithStatusJSON(statusCode, response)
}

func GrpcErrorResponse(c *gin.Context, err error, format string, args ...interface{}) {
	statusCode := codes.Internal
	message := fmt.Sprintf(format, args...)

	st, _ := status.FromError(err)

	if st != nil {
		statusCode = st.Code()
		message = st.Message()
	}

	httpStatusCode, ok := grpcToHttpCode[statusCode]
	if !ok {
		httpStatusCode = http.StatusInternalServerError
	}

	if httpStatusCode == http.StatusInternalServerError {
		logger.Get().Error(message)
		message = DefaultInternalErrorMessage
	} else {
		logger.Get().Warn(message)
	}

	response := model.ErrorResponse{
		Code:    httpStatusCode,
		Message: http.StatusText(httpStatusCode),
		Error:   message,
	}

	c.AbortWithStatusJSON(httpStatusCode, response)
}
