package util

import (
	"io"
	"mime/multipart"
	"net/http"
	"path/filepath"

	vars "gitlab.com/img_project/img_go_api_gateway/img_variables"
)

func ValidateFile(uploadedFile *multipart.FileHeader, extensions, mimeTypes []string) (multipart.File, error) {
	file, err := uploadedFile.Open()
	if err != nil {
		return nil, err
	}

	ext := filepath.Ext(uploadedFile.Filename)

	if !InArray(ext, extensions) {
		return nil, vars.ErrNotAllowedFile
	}

	buff := make([]byte, 512)
	_, err = file.Read(buff)
	if err != nil {
		return nil, err
	}

	filetype := http.DetectContentType(buff)
	if !InArray(filetype, mimeTypes) {
		return nil, vars.ErrNotAllowedFile
	}

	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}

	return file, nil
}
