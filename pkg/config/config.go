package config

import "github.com/MrWebUzb/goenv"

// Application configurations of application
type Application struct {
	Name     string `env:"APPLICATION" default:"example_service" required:"true"` // application name
	Port     string `env:"SERVICE_PORT" default:":9999"`                          // application running port
	LogLevel string `env:"LOG_LEVEL" default:"debug"`                             // application log level
}

// AuthService configurations
type AuthService struct {
	Host string `env:"AUTH_SERVICE_HOST"`
	Port int    `env:"AUTH_SERVICE_PORT"`
}

// UploadService configurations
type UploadService struct {
	Host string `env:"UPLOAD_SERVICE_HOST"`
	Port int    `env:"UPLOAD_SERVICE_PORT"`
}

// Config structure of application config parameters
// Application name of the service
// LogLevel level of the logging messages
type Config struct {
	Environment   string        // service environment
	App           Application   // application configs
	AuthService   AuthService   // auth service configs
	UploadService UploadService // upload service configs
}

// Instance of the configurations
var _config *Config = nil

func New(environment string, fileNames ...string) error {
	env, err := goenv.New(fileNames...)

	if err != nil {
		return err
	}

	if _config == nil {
		_config = &Config{
			Environment: environment,
		}

		if err := env.Parse(_config); err != nil {
			return err
		}
	}

	return nil
}

func Get() *Config {
	return _config
}
