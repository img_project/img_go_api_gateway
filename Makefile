include .env
CURRENT_DIR=$(shell pwd)

vendor:
	rm -rf vendor && go mod vendor

run:
	go run ./cmd/

run-client:
	go run ./cmd/client

script:
	go build ./cmd/script/

test:
	go test -v -timeout=30s ./...

build:
	go build -o app ./cmd/

gen-proto:
	./scripts/proto-gen.sh ${CURRENT_DIR} img_protos

migrate-up:
	migrate -path=${CURRENT_DIR}/migrations/postgres -database=postgres://${POSTGRES_USER}:${POSTGRES_PASS}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?sslmode=disable up

migrate-down:
	migrate -path=${CURRENT_DIR}/migrations/postgres -database=postgres://${POSTGRES_USER}:${POSTGRES_PASS}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?sslmode=disable down

swag:
	swag init -g handler/api.go --output docs/

.PHONY: run test build vendor
.DEFAULT_GOAL:=run