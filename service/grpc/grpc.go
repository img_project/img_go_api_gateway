package grpc

import (
	"gitlab.com/img_project/img_go_api_gateway/pkg/config"
	serviceI "gitlab.com/img_project/img_go_api_gateway/service"
)

type service struct {
	cfg      *config.Config
	services map[string]interface{}
}

func NewService() (serviceI.Service, error) {
	s := &service{cfg: config.Get()}

	s.services = map[string]interface{}{
		"auth_service":   s.authService(),
		"upload_service": s.uploadService(),
	}

	return s, nil
}
