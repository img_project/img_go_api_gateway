package grpc

import (
	"fmt"

	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	uploadServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/upload_service/v1"
	"google.golang.org/grpc"
)

func (s *service) UploadService() uploadServiceV1.UploadServiceClient {
	return s.services["upload_service"].(uploadServiceV1.UploadServiceClient)
}

func (s *service) uploadService() uploadServiceV1.UploadServiceClient {
	cfg := s.cfg.UploadService
	address := fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		logger.Get().Error("could not connect to upload service", logger.Error(err))
		panic(err)
	}

	client := uploadServiceV1.NewUploadServiceClient(conn)

	return client
}
