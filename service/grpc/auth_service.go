package grpc

import (
	"fmt"

	authServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/auth_service/v1"
	"google.golang.org/grpc"
)

func (s *service) AuthService() authServiceV1.AuthServiceClient {
	return s.services["auth_service"].(authServiceV1.AuthServiceClient)
}

func (s *service) authService() authServiceV1.AuthServiceClient {
	cfg := s.cfg.AuthService
	address := fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		panic(err)
	}

	client := authServiceV1.NewAuthServiceClient(conn)

	return client
}
