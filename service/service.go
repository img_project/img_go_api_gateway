package service

import (
	authServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/auth_service/v1"
	uploadServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/upload_service/v1"
)

type Service interface {
	AuthService() authServiceV1.AuthServiceClient
	UploadService() uploadServiceV1.UploadServiceClient
}
