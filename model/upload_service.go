package model

import (
	"mime/multipart"
)

type UploadImageRequest struct {
	Name string                `form:"name" binding:"required"`
	File *multipart.FileHeader `form:"file" binding:"required"`
}

type GetImageRequest struct {
	ID    string `uri:"id" binding:"required"`
	Width int64  `form:"width"`
}

type UploadImageResponse struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	FileName string `json:"file_name"`
}
