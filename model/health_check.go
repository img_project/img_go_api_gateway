package model

type PongResponse struct {
	Pong bool `json:"pong"`
}
