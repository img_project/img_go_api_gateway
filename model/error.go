package model

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
	Error   string `json:"error,omitempty"`
}
