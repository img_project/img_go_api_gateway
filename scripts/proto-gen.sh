#!/bin/bash

DIR=$1
INPUT_PATH=$2
OUTPUT_PATH=${DIR}/

rm -rf ${OUTPUT_PATH}/protobuf

for x in $(find ${DIR}/${INPUT_PATH}/* -type d); do
  if ls ${x}/*.proto &>/dev/null
  then
    protoc -I=${x} -I=${DIR}/${INPUT_PATH} -I /usr/local/include --go_out=plugins=grpc:${OUTPUT_PATH} ${x}/*.proto  
  fi
done
