package handler

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "gitlab.com/img_project/img_go_api_gateway/docs"
	v1 "gitlab.com/img_project/img_go_api_gateway/handler/v1"
	"gitlab.com/img_project/img_go_api_gateway/pkg/config"
)

type Api struct {
	router *gin.Engine
}

func (v1 *Api) Run() error {
	return v1.router.Run(config.Get().App.Port)
}

func (v1 *Api) GetRouter() *gin.Engine {
	return v1.router
}

// @title Image resizer API
// @version 1.0
// @description This is a sample microservice architecture application.
// @termsOfService http://swagger.io/terms/

// @BasePath /api/v1
// @schemes http https

// @contact.name API Support
// @contact.url https://t.me/middl3_dev
// @contact.email space.coding.programmer@gmail.com

// @query.collection.format multi

// @securityDefinitions.basic BasicAuth

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func NewApi() *Api {
	router := gin.New()

	if config.Get().Environment == "production" {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	router.Use(gin.Logger())

	router.Use(gin.Recovery())
	router.Use(cors.Default())

	handlerV1 := v1.NewHandlerV1()

	v1 := router.Group("/api/v1")
	{
		v1.GET("/ping", handlerV1.Ping)
		v1.POST("/login", handlerV1.Login)
		v1.POST("/refresh", handlerV1.RefreshToken)

		authRequired := v1.Group("/")
		authRequired.Use(handlerV1.AuthRequired)
		{
			authRequired.POST("/upload", handlerV1.Upload)
			authRequired.GET("/list", handlerV1.List)
			authRequired.GET("/:id", handlerV1.Image)
		}
	}

	url := ginSwagger.URL("docs/doc.json")
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return &Api{
		router: router,
	}
}
