package test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/img_project/img_go_api_gateway/model"
)

func TestRefreshToken(t *testing.T) {
	body, err := json.Marshal(model.LoginRequest{
		Username: "test_user",
		Password: "secret",
	})

	assert.NoError(t, err)

	r, err := http.NewRequest(http.MethodPost, "/api/v1/login", bytes.NewBuffer(body))

	assert.NoError(t, err)

	w := httptest.NewRecorder()

	api.GetRouter().ServeHTTP(w, r)

	res := model.SuccessResponse{}

	assert.NoError(t, json.Unmarshal(w.Body.Bytes(), &res))

	data, ok := res.Data.(map[string]interface{})

	assert.Equal(t, true, ok)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.NotEmpty(t, data["access_token"])
	assert.NotEmpty(t, data["refresh_token"])

	testCases := []struct {
		name         string
		path         string
		method       string
		expectedCode int
		valid        bool
		body         model.RefreshTokenRequest
	}{
		{
			name:         "correct token",
			path:         "/api/v1/refresh",
			method:       http.MethodPost,
			expectedCode: http.StatusOK,
			body: model.RefreshTokenRequest{
				RefreshToken: data["refresh_token"].(string),
			},
		},
		{
			name:         "incorrect refresh token",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusUnauthorized,
			body: model.RefreshTokenRequest{
				RefreshToken: "test",
			},
		},
		{
			name:         "request with sql injection",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusUnauthorized,
			body: model.RefreshTokenRequest{
				RefreshToken: "test' or 1=1--",
			},
		},
		{
			name:         "request with empty data",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusUnauthorized,
			body: model.RefreshTokenRequest{
				RefreshToken: "",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			body, err := json.Marshal(tc.body)

			assert.NoError(t, err)

			r, err := http.NewRequest(tc.method, tc.path, bytes.NewBuffer(body))

			assert.NoError(t, err)

			w := httptest.NewRecorder()

			api.GetRouter().ServeHTTP(w, r)

			assert.Equal(t, tc.expectedCode, w.Code)
		})
	}
}
