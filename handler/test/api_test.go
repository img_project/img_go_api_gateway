package test

import (
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/handler"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
)

var api *handler.Api

func TestMain(m *testing.M) {
	util.LoadConfig("test", "../../.env")

	api = handler.NewApi()

	gin.SetMode(gin.TestMode)

	os.Exit(m.Run())
}
