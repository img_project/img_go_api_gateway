package test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/img_project/img_go_api_gateway/model"
)

func TestLogin(t *testing.T) {
	testCases := []struct {
		name         string
		path         string
		method       string
		expectedCode int
		valid        bool
		body         model.LoginRequest
	}{
		{
			name:         "correct login",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusOK,
			body: model.LoginRequest{
				Username: "test_user",
				Password: "secret",
			},
		},
		{
			name:         "incorrect login",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusUnauthorized,
			body: model.LoginRequest{
				Username: "test",
				Password: "test",
			},
		},
		{
			name:         "request with sql injection",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusUnauthorized,
			body: model.LoginRequest{
				Username: "test' or 1=1--",
				Password: "test' or 1=1--",
			},
		},
		{
			name:         "request with empty data",
			path:         "/api/v1/login",
			method:       http.MethodPost,
			expectedCode: http.StatusBadRequest,
			body: model.LoginRequest{
				Username: "",
				Password: "",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			body, err := json.Marshal(tc.body)

			assert.NoError(t, err)

			r, err := http.NewRequest(tc.method, tc.path, bytes.NewBuffer(body))

			assert.NoError(t, err)

			w := httptest.NewRecorder()

			api.GetRouter().ServeHTTP(w, r)

			assert.Equal(t, tc.expectedCode, w.Code)
		})
	}
}
