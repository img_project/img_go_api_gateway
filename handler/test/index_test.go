package test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPing(t *testing.T) {
	testCases := []struct {
		name         string
		path         string
		method       string
		expectedCode int
		valid        bool
	}{
		{
			name:         "valid test case",
			path:         "/api/v1/ping",
			method:       http.MethodGet,
			expectedCode: http.StatusOK,
			valid:        true,
		},
		{
			name:         "invalid test case",
			path:         "/api/v1/ping",
			method:       http.MethodGet,
			expectedCode: http.StatusBadRequest,
			valid:        false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			r, err := http.NewRequest(tc.method, tc.path, nil)

			assert.NoError(t, err)

			w := httptest.NewRecorder()

			api.GetRouter().ServeHTTP(w, r)

			if tc.valid {
				assert.Equal(t, tc.expectedCode, w.Code)
			} else {
				assert.NotEqual(t, tc.expectedCode, w.Code)
			}
		})
	}
}
