package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	uploadServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/upload_service/v1"
)

// List images
// @Summary Checks service alive
// @Description Returns list of the user images
// @Accept	json
// @Produce	json
// @Param	offset	query	int	false	"Offset of the list"
// @Param	limit	query	int	false	"Limit of the list"
// @Success 200
// @Failure default {object} model.ErrorResponse
// @Router /list [get]
// @Tags image
// @Security ApiKeyAuth
func (v1 *HandlerV1) List(c *gin.Context) {
	logger.Get().Debug("requested to image list api")

	var query *model.PaginationQuery

	if err := c.ShouldBindQuery(&query); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", util.BindError(err))
		return
	}

	listImageRequest := &uploadServiceV1.ListImageRequest{
		UserId: v1.UserID,
		Offset: query.Offset,
		Limit:  query.Limit,
	}

	listImageResponse, err := v1.grpcService.UploadService().List(c, listImageRequest)

	if err != nil {
		util.GrpcErrorResponse(c, err, "could not get list of the images: %v", err)
		return
	}

	util.SuccessResponse(c, http.StatusOK, listImageResponse)
}
