package v1

import (
	"bytes"
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	uploadServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/upload_service/v1"
)

// Image returns image
// @Summary Checks service alive
// @Description Returns user uploaded image
// @Accept	json
// @Produce	jpeg
// @Param	image_id	path	string	true	"image_id"
// @Param	width	query	int		false	"width"
// @Success 200 {object} object "The requested image"
// @Failure default {object} model.ErrorResponse
// @Router /{image_id} [get]
// @Tags image
// @Security ApiKeyAuth
func (v1 *HandlerV1) Image(c *gin.Context) {
	logger.Get().Debug("requested to get image api")
	var getImageRequest model.GetImageRequest

	if err := c.ShouldBindUri(&getImageRequest); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", err)
		return
	}

	if err := c.ShouldBindQuery(&getImageRequest); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", err)
		return
	}

	if !util.ValidateUUID(getImageRequest.ID) {
		util.ErrorResponse(c, http.StatusNotFound, "image not found")
		return
	}

	request := &uploadServiceV1.GetImageRequest{
		Id:     getImageRequest.ID,
		UserId: v1.UserID,
		Width: func(num int64) *int64 {
			if num > 0 {
				return &num
			}

			return nil
		}(getImageRequest.Width),
	}

	stream, err := v1.grpcService.UploadService().Get(c, request)

	if err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "error getting file: %v", err)
		return
	}

	fileData, fileSize := bytes.Buffer{}, 0

	for {
		err := stream.Context().Err()

		if err != nil {
			util.GrpcErrorResponse(c, err, "context error: %v", err)
			return
		}

		logger.Get().Info("receiving data")

		response, err := stream.Recv()
		if err == io.EOF {
			logger.Get().Info("finished reading data")
			break
		}

		if err != nil {
			util.GrpcErrorResponse(c, err, "could not receive data from service: %v", err)
			return
		}

		chunk := response.GetContent()
		size := len(chunk)

		logger.Get().Info("received chunk with size of ", logger.Int("size", size))

		fileSize += size

		_, err = fileData.Write(chunk)
		if err != nil {
			util.ErrorResponse(c, http.StatusInternalServerError, "could not write file to buffer: %v", err)
			return
		}

		_, err = c.Writer.Write(fileData.Bytes())

		if err != nil {
			util.ErrorResponse(c, http.StatusInternalServerError, "could not write data to response: %v", err)
			return
		}
	}

	_, err = c.Writer.Write(fileData.Bytes())

	if err != nil {
		util.ErrorResponse(c, http.StatusInternalServerError, "could not write data to response: %v", err)
		return
	}
}
