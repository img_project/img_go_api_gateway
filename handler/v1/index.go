package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
)

// Ping godoc
// @Summary Checks service alive
// @Description Returns pong response
// @Accept  json
// @Produce  json
// @Success 200 {object} model.SuccessResponse{data=model.PongResponse} "Returns true if all services are alive"
// @Tags alive
// @Router /ping [get]
func (v1 *HandlerV1) Ping(c *gin.Context) {
	logger.Get().Debug("requested to ping api")

	util.SuccessResponse(c, http.StatusOK, gin.H{
		"pong": true,
	})
}
