package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	authServiceProto "gitlab.com/img_project/img_go_api_gateway/protobuf/auth_service/v1"
)

func (v1 *HandlerV1) AuthRequired(c *gin.Context) {
	accessToken := c.Request.Header.Get("Authorization")

	if accessToken == "" {
		util.ErrorResponse(c, http.StatusUnauthorized, "please specify access token")
		return
	}

	authServiceVerifyRequest := &authServiceProto.VerifyTokenRequest{
		Token: accessToken,
	}

	authServiceVerifyResponse, err := v1.grpcService.AuthService().VerifyToken(c, authServiceVerifyRequest)

	if err != nil {
		util.GrpcErrorResponse(c, err, "authentication failed: %v", err)
		return
	}

	if authServiceVerifyResponse.Valid {
		v1.UserID = authServiceVerifyResponse.UserId
		logger.Get().Debug("logged user id in middleware", logger.String("userID", authServiceVerifyResponse.GetUserId()))
	} else {
		util.ErrorResponse(c, http.StatusUnauthorized, "invalid access token")
		return
	}

	c.Next()
}
