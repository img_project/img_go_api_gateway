package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	authServiceProto "gitlab.com/img_project/img_go_api_gateway/protobuf/auth_service/v1"
)

// Login godoc
// @Summary Authorizing for exisiting user
// @Description Returns access token response
// @Accept  json
// @Produce  json
// @Param loginRequest body model.LoginRequest true "Auth credentials"
// @Success 200 {object} model.SuccessResponse{data=model.TokenResponse}
// @Failure default {object} model.ErrorResponse
// @Tags auth
// @Router /login [post]
func (v1 *HandlerV1) Login(c *gin.Context) {
	logger.Get().Debug("requested to login api")

	var loginRequest *model.LoginRequest

	if err := c.BindJSON(&loginRequest); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", util.BindError(err))
		return
	}

	authLoginRequest := &authServiceProto.LoginRequest{
		Username: loginRequest.Username,
		Password: loginRequest.Password,
	}

	authTokenResponse, err := v1.grpcService.AuthService().Login(c, authLoginRequest)

	if err != nil {
		util.GrpcErrorResponse(c, err, "authentication failed: %v", err)
		return
	}

	util.SuccessResponse(c, http.StatusOK, model.TokenResponse{
		AccessToken:  authTokenResponse.GetAccessToken(),
		RefreshToken: authTokenResponse.GetRefreshToken(),
	})
}
