package v1

import (
	"bufio"
	"context"
	"io"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	vars "gitlab.com/img_project/img_go_api_gateway/img_variables"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	uploadServiceV1 "gitlab.com/img_project/img_go_api_gateway/protobuf/upload_service/v1"
)

// Upload godoc
// @Summary Upload image to the server
// @Description Uploads image
// @Accept  mpfd
// @Produce  json
// @Param file formData file true "file"
// @Param name formData string true "name"
// @Success 200 {object} model.SuccessResponse{data=model.UploadImageResponse} "Returns true if all services are alive"
// @Tags image
// @Router /upload [post]
// @Security ApiKeyAuth
func (v1 *HandlerV1) Upload(c *gin.Context) {
	logger.Get().Debug("requested to upload api")

	var uploadRequest model.UploadImageRequest

	if err := c.ShouldBind(&uploadRequest); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", util.BindError(err))
		return
	}

	file, err := util.ValidateFile(uploadRequest.File, vars.AllowedExtensions, vars.AllowedMimeTypes)

	if err != nil {
		if err == vars.ErrNotAllowedFile {
			util.ErrorResponse(c, http.StatusBadRequest, "only allowed: %s", strings.Join(vars.AllowedExtensions, ","))
			return
		}
		if err == io.EOF {
			util.ErrorResponse(c, http.StatusBadRequest, "empty file")
			return
		}
		util.ErrorResponse(c, http.StatusInternalServerError, "could not save file: %v", err)
		return
	}

	stream, err := v1.grpcService.UploadService().Upload(context.Background())
	if err != nil {
		util.ErrorResponse(c, http.StatusInternalServerError, "could not save file: %v", err)
		return
	}

	request := &uploadServiceV1.UploadImageRequest{
		Data: &uploadServiceV1.UploadImageRequest_Info{
			Info: &uploadServiceV1.UploadImageInfo{
				FileName: uploadRequest.Name,
				FileType: filepath.Ext(uploadRequest.File.Filename),
				UserId:   v1.UserID,
			},
		},
	}

	if err := stream.Send(request); err != nil {
		util.GrpcErrorResponse(c, err, "could not save file: %v", err)
		return
	}

	reader := bufio.NewReader(file)
	buffer := make([]byte, vars.DefaultChunkSize)
	size := 0

	for {
		n, err := reader.Read(buffer)
		if err == io.EOF {
			break
		}

		if err != nil {
			util.ErrorResponse(c, http.StatusInternalServerError, "could not save file: %v", err)
			return
		}

		size += n

		request := &uploadServiceV1.UploadImageRequest{
			Data: &uploadServiceV1.UploadImageRequest_Chunk{
				Chunk: buffer[:n],
			},
		}

		err = stream.Send(request)

		if err != nil {
			util.ErrorResponse(c, http.StatusInternalServerError, "could not save file: %v", err)
			return
		}
	}

	response, err := stream.CloseAndRecv()
	if err != nil {
		util.ErrorResponse(c, http.StatusInternalServerError, "could not save file: %v", err)
		return
	}

	util.SuccessResponse(c, http.StatusCreated, model.UploadImageResponse{
		ID:       response.Id,
		Name:     response.Name,
		FileName: response.FileName,
	})
}
