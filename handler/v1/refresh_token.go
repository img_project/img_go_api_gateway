package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/img_project/img_go_api_gateway/model"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
	"gitlab.com/img_project/img_go_api_gateway/pkg/util"
	authServiceProto "gitlab.com/img_project/img_go_api_gateway/protobuf/auth_service/v1"
)

// RefreshToken godoc
// @Summary Authorizing for exisiting user
// @Description Returns new access token response
// @Accept  json
// @Produce  json
// @Param refreshTokenRequest body model.RefreshTokenRequest true "Refresh token"
// @Success 200 {object} model.SuccessResponse{data=model.TokenResponse}
// @Failure default {object} model.ErrorResponse
// @Tags auth
// @Router /refresh [post]
// @Security ApiKeyAuth
func (v1 *HandlerV1) RefreshToken(c *gin.Context) {
	logger.Get().Debug("requested to refresh token api")

	var refreshTokenRequest *model.RefreshTokenRequest

	if err := c.BindJSON(&refreshTokenRequest); err != nil {
		util.ErrorResponse(c, http.StatusBadRequest, "%v", util.BindError(err))
		return
	}

	authRefreshTokenRequest := &authServiceProto.RefreshTokenRequest{
		RefreshToken: refreshTokenRequest.RefreshToken,
	}

	authTokenResponse, err := v1.grpcService.AuthService().RefreshToken(c, authRefreshTokenRequest)

	if err != nil {
		util.GrpcErrorResponse(c, err, "could not get new tokens: %v", err)
		return
	}

	util.SuccessResponse(c, http.StatusOK, model.TokenResponse{
		AccessToken:  authTokenResponse.GetAccessToken(),
		RefreshToken: authTokenResponse.GetRefreshToken(),
	})
}
