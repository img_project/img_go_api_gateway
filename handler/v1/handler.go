package v1

import (
	serviceI "gitlab.com/img_project/img_go_api_gateway/service"
	grpcI "gitlab.com/img_project/img_go_api_gateway/service/grpc"
)

type HandlerV1 struct {
	grpcService serviceI.Service
	UserID      string
}

func NewHandlerV1() *HandlerV1 {
	serv, err := grpcI.NewService()

	if err != nil {
		panic(err)
	}

	return &HandlerV1{
		grpcService: serv,
	}
}
