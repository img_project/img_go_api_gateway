package main

import (
	"os"

	"gitlab.com/img_project/img_go_api_gateway/handler"
	"gitlab.com/img_project/img_go_api_gateway/pkg/config"
	"gitlab.com/img_project/img_go_api_gateway/pkg/logger"
)

func main() {
	// Load config from .env
	if err := config.New(os.Getenv("ENVIRONMENT")); err != nil {
		panic(err)
	}

	// Initialize customized zap logger
	logger.New(config.Get().App.LogLevel, config.Get().App.Name)

	// Initialize http server
	api := handler.NewApi()

	if err := api.Run(); err != nil {
		panic(err)
	}
}
